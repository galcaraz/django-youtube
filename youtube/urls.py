"""Youtube app resource configuration
"""
from django.urls import path
from . import views

urlpatterns = [
    path('<str:id_req>', views.get_video),
    path('', views.main, name='main')
]
